<?php
 
namespace App\Http\Controllers;
 
use App\Employee;
use Illuminate\Http\Request;
 
class EmployeeController extends Controller
{
    public function index()
    {
        $employees = auth()->user()->employees;
 
        return response()->json([
            'success' => true,
            'data' => $employees
        ]);
    }
 
    public function show($id)
    {
        $employee = auth()->user()->employees()->find($id);
 
        if (!$employee) {
            return response()->json([
                'success' => false,
                'message' => 'Employee with id ' . $id . ' not found'
            ], 400);
        }
 
        return response()->json([
            'success' => true,
            'data' => $employee->toArray()
        ], 400);
    }
 
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'full_name' => 'required'
        ]);
 
        $employee = new Employee();
        $employee->email = $request->email;
        $employee->full_name = $request->full_name;
 
        if (auth()->user()->employees()->save($employee))
            return response()->json([
                'success' => true,
                'data' => $employee->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Employee could not be added'
            ], 500);
    }
 
    public function update(Request $request, $id)
    {
        $employee = auth()->user()->employees()->find($id);
 
        if (!$employee) {
            return response()->json([
                'success' => false,
                'message' => 'Employee with id ' . $id . ' not found'
            ], 400);
        }
 
        $updated = $employee->update($request->all());
 
        if ($updated)
            return response()->json([
                'success' => true,
                'req' => $request->all()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Employee could not be updated'
            ], 500);
    }
 
    public function destroy($id)
    {
        $employee = auth()->user()->employees()->find($id);
 
        if (!$employee) {
            return response()->json([
                'success' => false,
                'message' => 'Employee with id ' . $id . ' not found'
            ], 400);
        }
 
        if ($employee->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Employee could not be deleted'
            ], 500);
        }
    }
}